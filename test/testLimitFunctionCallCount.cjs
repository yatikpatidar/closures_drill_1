const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

let result = limitFunctionCallCount((argument) => {
    console.log("callback invoked ", argument)
}, 3)

result()
result()
result()
result()
result()

