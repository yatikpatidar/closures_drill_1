const cacheFunction = require('../cacheFunction.cjs')

let result = cacheFunction((argument)=>{
    return argument*2                       // in callback we are doubling the argument
})

console.log(result(2))
console.log(result(4))
console.log(result(20))
console.log(result(10))
console.log(result(4))
console.log(result(5))