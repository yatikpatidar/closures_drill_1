// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {

    return function () {
        if (n > 0) {
            n -= 1
            return cb(n + 1);
        } else {
            console.log("invoking callback function limit already reached ! ")
            return null;
        }
    }
}

module.exports = limitFunctionCallCount